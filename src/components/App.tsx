import * as React from "react";
import './App.scss';
import { Dashboard } from "./dashboard/dashboard.pure.screen";

// import 'antd/dist/antd.css';

class App extends React.Component {

    render() {
        return (
            <Dashboard />
        )
    }
}

export default App;