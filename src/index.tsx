import "core-js";
import "regenerator-runtime/runtime";
import * as React from "react";
import * as ReactDOM from "react-dom";
import App from "./components/App";
import { createStore } from 'redux'
import { Provider } from 'react-redux';

import '../index.scss'

const store = createStore();


ReactDOM.render(<App />, document.getElementById("app"));